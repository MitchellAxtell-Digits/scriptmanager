#!/bin/bash

##This will determine if anything is needed from a stock Ubuntu on Windows 16.04 install
##Please also make sure that the install is up to date, as this is assumed

##Setting the client ID for the API here for easy reference, as well as the config file version
	ClientID="4a218db74c58aac95ee1cc9633dbfc16"
	ConfigVersion="3"

exec >/dev/null

##Gather status of prerequisites- "ii" indicates installed
	pythonstatus=`dpkg -l | grep -i "ii  python3 "`
	pipstatus=`dpkg -l | grep -i "ii  python3-pip "`
	gitstatus=`dpkg -l | grep -i "ii  git "`

##First, check for updates to the script if Git is present
	if [[ $gitstatus == *ii* ]] ; then
		ScriptUpdate=`git -C ${BASH_SOURCE%/*} pull`
		if [[ $ScriptUpdate == *"Updating"* ]] ; then :
			echo >&2 "Script updated, see below. Please re-run to activate the newer version"
			echo >&2 "$ScriptUpdate"
			exit 1
			elif [[ $ScriptUpdate == *"Already up to date"* ]] ; then :
			else 
				echo >&2 "Cannot determine if the script needs an update- please see below"
				echo >&2 "$ScriptUpdate"
				exit 1
		fi 
		else :
	fi

##Check if the above items are installed, and if not add to a command output. Setting the variable to a negative to start- will overwrite if needed
	PackagestoInstall="No packages to install- prerequisites should be present"
	if [[ "$pythonstatus" != *ii* ]] || [[ "$pipstatus" != *ii* ]] || [[ "$gitstatus" != *ii* ]]; then 
		PackagestoInstall="The following command should be run to install necessary prerequisites"
		PackagestoInstall+=$'\n'
		PackagestoInstall+="sudo apt-get install"
		if [[ $pythonstatus != *ii* ]] ; then PackagestoInstall+=" python3"
			else :
		fi
		if [[ $pipstatus != *ii* ]] ; then PackagestoInstall+=" python3-pip" ; PackagestoInstall+=$'\n' ; PackagestoInstall+="Please re-run this script after installing pip to complete checks"
			else :
		fi
		if [[ $gitstatus != *ii* ]] ; then PackagestoInstall+=" git" ; PackagestoInstall+=$'\n' ; PackagestoInstall+="Please re-run this script after installing git to complete checks"
			else :
		fi
		else :
	fi

##Check the pip items are present, and if not add to a command output. Setting the variable to a negative to start- will overwrite if needed
	PIPInstall=""
	if [[ $pipstatus == *ii* ]] ; then
	  echo >&2 "Installing and updating PIP prerequisites for Python"
		echo >&2 "Please ignore any lines that start with 'You' (should be the next two lines)- these are system output and not recommended"
		pipinstall=`python3 -m pip install --upgrade pip datetime joblib netmiko requests 'packaging==21.3' pytz pexpect pysnmp python-dateutil pyyaml timezonefinder --user`
		else :
	fi

echo >&2 "$PackagestoInstall"
echo >&2 "$PIPInstall"

##Generate the config file for the API info
	if test -e ~/.scriptconfig.txt; then
		##Determine the config file version
			ConfigFileVersion=`grep "ConfigVersion" ~/.scriptconfig.txt | cut -f3 -d' '`
			echo >&2 "Configuration file is present- file version is $ConfigFileVersion"
		else echo >&2 "Generating configuration file- please go to the following URL to authorize an API token (should automatically open)"
			##Get the URL for the token authorization and send it to the client
				AuthURL=`curl -s --header "Accept:application/json" "https://support.aws.opennetworkexchange.net/api/v1/oauth/token?scope=view&client_id=$ClientID" | cut -f4 -d'"' | sed 's/\///g'`
				echo >&2 ""
				cmd.exe /c start $AuthURL
				echo >&2 "	$AuthURL"
				echo >&2 ""
				echo >&2 "The post auth redirect will not work, but this is expected as it redirects to hotspot.singledigits.net (the API token still works)"
			
			##Get the auth token from the user
				echo >&2 "Please input everything in the redirected URL after code= (should be a 32 character code)"
				echo >&2 ""
				read -p 'code=' AuthToken
			
			##Wait a little, test access and if successful inform about a few caveats
				sleep 2
				TestResult=`curl -s --header "Accept:application/json" "https://api.aws.opennetworkexchange.net/api/v2/accounts/2214?access_token=$AuthToken&client_id=$ClientID"`
				if [[ "$TestResult" == *":2214,"* ]] ; then echo >&2 "Token tested successfully. If you need to generate a new token, please delete ~/.scriptconfig.txt and try again"
					else echo >&2 "Unable to authenticate successfully with this token- please try again. The result is $TestResult" ; rm ~/.scriptconfig.txt
				fi
			##Set the config version to 0 to indicate everything needs to be done
				ConfigFileVersion="0"
	fi

##Check the config version to see if anything needs to be added
	if [[ $ConfigFileVersion -lt $ConfigVersion ]] ; then
		if [[ $ConfigFileVersion -lt "1" ]] ; then
			##Write out the info to file
				echo "[configuration]" > ~/.scriptconfig.txt
				echo "ConfigVersion = $ConfigVersion" >> ~/.scriptconfig.txt
				echo "ClientID = $ClientID" >> ~/.scriptconfig.txt
				echo "AuthToken = $AuthToken" >> ~/.scriptconfig.txt
			else :
		fi
		if [[ $ConfigFileVersion -lt "2" ]] ; then
			##Change the config file version in the file
				sed -i "s/ConfigVersion = ./ConfigVersion = $ConfigVersion/" ~/.scriptconfig.txt
			
			##Get the default username and password for devices
				echo >&2 ""
				echo >&2 "Please enter a standard username and password for devices (such as admin/password1234). TACACS will be prompted for later"
				echo >&2 ""
				read -p 'Standard username=' StandardUsername
				read -p 'Standard password=' -s StandardPassword

			##Get the TACACS username and password for devices
				echo >&2 ""
				echo >&2 "Please enter a TACACS username and password for devices (such as admin/password1234)"
				echo >&2 ""
				read -p 'TACACS username=' TACACSUsername
				read -p 'TACACS password=' -s TACACSPassword

			##Write out the info to file
				echo "StandardUsername = $StandardUsername" >> ~/.scriptconfig.txt
				echo "StandardPassword = $StandardPassword" >> ~/.scriptconfig.txt
				echo "TACACSUsername = $TACACSUsername" >> ~/.scriptconfig.txt
				echo "TACACSPassword = $TACACSPassword" >> ~/.scriptconfig.txt

			else :
		fi
		if [[ $ConfigFileVersion -lt "3" ]] ; then
			##Change the config file version in the file
				sed -i "s/ConfigVersion = ./ConfigVersion = $ConfigVersion/" ~/.scriptconfig.txt

      ##Get the current passwords from the file if not already present
        if [[ -n $TACACSPassword ]] ; then :
          else :
            StandardPassword=`grep "StandardPassword" ~/.scriptconfig.txt | cut -f3 -d' '`
            TACACSPassword=`grep "TACACSPassword" ~/.scriptconfig.txt | cut -f3 -d' '`
        fi

      ##Convert the passwords with base64 encoded ones to obfuscate readability
        ConvertedStandardPassword=`echo -n "$StandardPassword" | base64`
        ConvertedTACACSPassword=`echo -n "$TACACSPassword" | base64`

      ##Insert the modified password into the file
        sed -i "s/StandardPassword = .*/StandardPassword = $ConvertedStandardPassword/" ~/.scriptconfig.txt
        sed -i "s/TACACSPassword = .*/TACACSPassword = $ConvertedTACACSPassword/" ~/.scriptconfig.txt

      else :
    fi

		else :
	fi

##Check if the logging configuration file needs to be created
		if test -e ~/.loggingconfig.conf; then :
		else
			echo -e "[loggers]\nkeys=root\n\n[logger_root]\nlevel=DEBUG\nhandlers=streamHandle,fileHandle,debugFileHandle\n\n[formatters]\nkeys=streamForm,fileForm\n\n[formatter_streamForm]\nformat=%(name)s - %(levelname)s - %(message)s\nclass=logging.Formatter\n\n[formatter_fileForm]\nformat=%(asctime)s - %(name)s - %(levelname)s - %(message)s\nclass=logging.Formatter\n\n[handlers]\nkeys=streamHandle,fileHandle,debugFileHandle\n\n[handler_streamHandle]\nclass=StreamHandler\nlevel=ERROR\nformatter=streamForm\nargs=(sys.stdout,)\n\n[handler_fileHandle]\nclass=FileHandler\nlevel=INFO\nformatter=fileForm\nargs=('$HOME/.scriptlog.log', 'a')\n\n[handler_debugFileHandle]\nclass=FileHandler\nlevel=DEBUG\nformatter=fileForm\nargs=('$HOME/.debugscriptlog.log', 'a')">> ~/.loggingconfig.conf
			echo >&2 "Created logging configuration"
		fi

##Download requested scripts if Git is installed
	if [[ $gitstatus == *ii* ]] ; then
		echo >&2 ""
		echo >&2 "Need to download or update scripts?"
		read -p 'yes/no: ' ScriptDownload
		
		if [[ $ScriptDownload == *y* ]] ; then
			if [[ ! -d "mremote-csv-generator" ]] ; then
				echo >&2 "Download Mremote CSV Generator? This creates MRemote import files based on HSO numbers"
				read -p 'yes/no: ' ScriptDownload
				if [[ $ScriptDownload == *y* ]] ; then
					git clone -q https://bitbucket.org/MitchellAxtell-Digits/mremote-csv-generator.git
					else :
				fi
				else
					rm -rf mremote-csv-generator
					git clone -q https://bitbucket.org/MitchellAxtell-Digits/mremote-csv-generator.git
					echo >&2 "Updated Mremote CSV Generator"
			fi
			if [[ ! -d "fetch" ]] ; then
				echo >&2 "Download Fetch? This gathers information from all HSOs based on device types"
				read -p 'yes/no: ' ScriptDownload
				if [[ $ScriptDownload == *y* ]] ; then
					git clone -q https://bitbucket.org/MitchellAxtell-Digits/fetch.git
					else :
				fi
				else
					rm -rf fetch
					git clone -q https://bitbucket.org/MitchellAxtell-Digits/fetch.git
					echo >&2 "Updated Fetch"
			fi
			if [[ ! -d "overnighttools" ]] ; then
				echo >&2 "Download OvernightTools? This has device-based functions such as identification, backups and updates"
				read -p 'yes/no: ' ScriptDownload
				if [[ $ScriptDownload == *y* ]] ; then
					git clone -q https://bitbucket.org/MitchellAxtell-Digits/overnighttools.git
					else :
				fi
				else
					rm -rf overnighttools
					git clone -q https://bitbucket.org/MitchellAxtell-Digits/overnighttools.git
					echo >&2 "Updated OvernightTools"
			fi
			echo >&2 "No more scripts to download- all are present"

			else 
				echo >&2 "OK, goodbye!"
		fi
		else :
	fi